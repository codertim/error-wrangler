
package net.timforce.dao;

/** 
 * 
 */  
  
import javax.sql.DataSource;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.jdbc.core.JdbcTemplate;  
import net.timforce.ErrorMessage;
import net.timforce.dao.ErrorMessageDao;

 
public class ErrorMessageDaoImpl implements ErrorMessageDao {  

	@Autowired
	DataSource dataSource;

	public void insertData(ErrorMessage errorMessage) {
		String sql = "INSERT INTO error_messages " + "(message) VALUES (?)";  
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql, new Object[] { errorMessage.getMessage() });
	}
}

