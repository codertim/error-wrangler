package net.timforce;
 
import net.timforce.ErrorMessage;
import net.timforce.dao.ErrorMessageDao;

import org.springframework.beans.factory.annotation.Autowired;   
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
@SessionAttributes
public class ErrorMessageController {
    @Autowired
    ErrorMessageDao errorMessageDao;
    
 
    @RequestMapping(value = "/addErrorMessage", method = RequestMethod.POST)
    public String addErrorMessage(@ModelAttribute("errorMessage")
                            ErrorMessage errorMessage, BindingResult result) {
        System.out.println("ErrorMessageController#addErrorMessage ... "); 

        System.out.println("Error Message:" + errorMessage.getMessage() +
                    "   Tags:" + errorMessage.getTags());
	errorMessageDao.insertData(errorMessage);

        return "redirect:errorMessages.html";
    }


    @RequestMapping(value = "/errorMessage", method = RequestMethod.GET)
    public ModelAndView errorMessage(@ModelAttribute("errorMessage") ErrorMessage errorMessage, BindingResult result) {
        System.out.println("ErrorMessageController#errorMessage ... "); 
        // return "errorMessage";
	return new ModelAndView("errorMessage", "command", new ErrorMessage());
    }

 
    @RequestMapping("/errorMessages")
    public String showErrorMessages() {
        System.out.println("ErrorMessageController#showErrorMessages ... "); 
        // return new ModelAndView("errorMessages", "command", new ErrorMessage());
        return "errorMessages";
    }
}


