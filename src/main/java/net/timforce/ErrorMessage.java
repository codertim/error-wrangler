
package net.timforce;


public class ErrorMessage {
	private String message;
	private String tags;
	private String solution;
	private String notes;

	public void setMessage(String message)   { this.message = message; }
	public void setTags(String tags)         { this.tags = tags; }
	public void setSolution(String solution) { this.solution = solution; }
	public void setNotes(String nots)        { this.notes = notes; }

	public String getMessage()   { return this.message; }
	public String getTags()      { return this.tags; }
	public String getSolution()  { return this.solution; }
	public String getNotes()     { return this.notes; }

}

