<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
  <head>
    <title>Spring 3 MVC - Error Message</title>
  </head>
  <body>
  <h1>Error Message</h1>
  <h2>Enter details</h2>
  <form:form method="post" action="addErrorMessage.html">
 
    <table>
    <tr>
        <td><form:label path="message">Message</form:label></td>
        <td><form:input path="message" /></td>
    </tr>
    <tr>
        <td><form:label path="tags">Tags (comma separated)</form:label></td>
        <td><form:input path="tags" /></td>
    </tr>
    <tr>
        <td><form:label path="solution">Solution</form:label></td>
        <td><form:input path="solution" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Add Error Message"/>
        </td>
    </tr>
  </table> 
     
  </form:form>
  </body>
</html>

